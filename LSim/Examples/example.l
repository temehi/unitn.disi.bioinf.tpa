A {}
B {}
C {}

pattern PA = [A]
pattern PB = [B]
pattern PAB = [A,B]

#print* PAB                        

dyn [A] [B] rate 3.32E-7 react
reagent_1.kill();
spawn(1:C);
end

run 30110:[1:A] 30110:[1:B]
