RAGE{}
RAGEa{}
RAS{}
RASa{}
PI3K{}
PI3Ka{}
PIP2{}
PIP3{}
PTEN{}
P53{}
MDM2{}
MDM2p{}
mdm2{}
ARF{}
E2F{}
Myc{}
CD{}
RE{}
RB{}
RBp{}
CE{}
ERK{}
ERKp{}
MEK{}
MEKp{}
RAF{}
RAFa{}
P21{}
INK4A{}
AKT{}
AKTp{}

pattern PRAGE = [RAGE]
pattern PRAGEa = [RAGEa]
pattern PRAS = [RAS]
pattern PRASa = [RASa]
pattern PPI3K = [PI3K]
pattern PPI3Ka = [PI3Ka]
pattern PPIP2 = [PIP2]
pattern PPIP3 = [PIP3]
pattern PPTEN = [PTEN]
pattern PP53 = [P53]
pattern PMDM2 = [MDM2]
pattern PMDM2p = [MDM2p]
pattern Pmdm2 = [mdm2]
pattern PARF = [ARF]
pattern PE2F = [E2F]
pattern PMyc = [Myc]
pattern PCD = [CD]
pattern PRE = [RE]
pattern PRB = [RB]
pattern PRBp = [RBp]
pattern PCE = [CE]
pattern PERK = [ERK]
pattern PERKp = [ERKp]
pattern PMEK = [MEK]
pattern PMEKp = [MEKp]
pattern PRAF = [RAF]
pattern PRAFa = [RAFa]
pattern PP21 = [P21]
pattern PINK4A = [INK4A]
pattern PAKT = [AKT]
pattern PAKTp = [AKTp]

#print* PRAGE
#print* PRAGEa
#print* PRAS
#print* PRASa
#print* PPI3K
#print* PPI3Ka
#print* PPTEN
#print* PP53
#print* PMDM2
#print* PMDM2p
#print* Pmdm2
#print* PARF
#print* PE2F
#print* PMyc
#print* PCD
#print* PRE
#print* PRB
#print* PRBp
#print* PCE
#print* PERK
#print* PERKp
#print* PMEK
#print* PMEKp
#print* PRAF
#print* PRAFa
#print* PP21
#print* PINK4A
#print* PAKT
#print* PAKTp
#print* PDNAD
#print* PDNA

#1 - HMGB1 + RAGE  -> HMGB1 + RAGEa -- k1
dyn [RAGE] rate 2e-5*1e3 react
	spawn(1:RAGEa);
	reagent_1.kill();
end
#2 - RAGEa -> RAGE -- d1
dyn [RAGEa] rate 2e-2 react
	spawn(1:RAGE);
	reagent_1.kill();
end
#3 - RAGEa + PI3K -> RAGEa + PI3Ka -- k2
dyn [PI3K] [RAGEa] rate 5.4E-5 react
	spawn(1:PI3Ka);
	reagent_1.kill();
end
#4 - PI3Ka -> PI3K -- d2
dyn [PI3Ka] rate 5E-3 react
	spawn(1:PI3K);
	reagent_1.kill();
end
#5 - PI3Ka + PIP2  ->  PI3Ka + PIP3 -- k3
dyn [PIP2] [PI3Ka] rate 3E-06 react
	spawn(1:PIP3);
	reagent_1.kill();
end
#6 - PTEN + PIP3 -> PTEN + PIP2 -- d3
dyn [PIP3] [PTEN] rate 3.5E-5 react
	spawn(1:PIP2);
	reagent_1.kill();
end
#7 - PIP3 + AKT -> PIP3 + AKTp -- k4
dyn [AKT] [PIP3] rate 1.2E-7 react
	spawn(1:AKTp);
	reagent_1.kill();
end
#8 - AKTp -> AKT -- d4
dyn [AKTp] rate 1.2E-2 react
	reagent_1.kill();
	spawn(1:AKT);
end
#9 - AKTp + MDM2 -> AKTp + MDM2p -- k8
dyn [MDM2] [AKTp] rate 4E-7 react
	spawn(1:MDM2p);
	reagent_1.kill();
end
#10 - MDM2p -> MDM2 -- d8
dyn [MDM2p] rate 3E-2 react
	spawn(1:MDM2);
	reagent_1.kill();
end
#11 - MDM2 -> deg -- d7
dyn [MDM2] rate 1.4E-2 react
	reagent_1.kill();
end
#12 - MDM2p -> deg -- d8'
dyn [MDM2p] rate 1.3E-2 react
	reagent_1.kill();
end
#13 - mdm2 -> mdm2 + MDM2 -- k7
dyn [mdm2] rate 30 react
	spawn(1:MDM2);
end
#14 - mdm2 -> deg -- d7
dyn [mdm2] rate 1.4E-2 react
	reagent_1.kill();
end
#15 - P53 -> P53 + mdm2 -- Hill(k6,Kb,n3)
dyn [P53] [P53] [P53] rate 32/(5.12E14 + (count [P53])*(count [P53])*(count [P53])) react
	spawn(1:mdm2);
end
#16 - I() -> I() + P53 -- k9
dyn rate 1.2E3 react
	spawn(1:P53);
end
#17 - P53 -> deg -- d9
dyn [P53] rate 1.2E-2 react
	reagent_1.kill();
end
#18 - MDM2p +  P53 -> MDM2p -- d9'
dyn [P53] [MDM2p] rate 6E-7 react
	reagent_1.kill();
end
#19 - P53 -> P53 + PTEN -- Hill(k5,Ka,n3)
dyn [P53] [P53] [P53] rate 28/(5.12E14+(count [P53])*(count [P53])*(count [P53])) react
	spawn(1:PTEN);
end
#20 PTEN -> deg -- d5
dyn [PTEN] rate 6E-3 react
	reagent_1.kill();
end
#21 - P53 -> P53 + P21 -- Hill(k10,Kc,n2)
dyn [P53] [P53] rate 8/(6.4E9+(count [P53])*(count [P53])) react
	spawn(1:P21);
end
#22 - P21 -> deg -- d10
dyn [P21] rate 1.8E-2 react
	reagent_1.kill();
end
#23 - RAGEa + RAS -> RAGEa + RASa -- a1\
dyn [RAS] [RAGEa] rate 2E-5 react
	spawn(1:RASa);
	reagent_1.kill();
end
#24 - RASa + PI3K -> RASa + PI3Ka -- k2'
dyn [PI3K] [RASa] rate 3E-7 react
	spawn(1:PI3Ka);
	reagent_1.kill();
end
#25 - RASa -> RAS -- b1
dyn [RASa] rate 1.6E-3 react
	spawn(1:RAS);
	reagent_1.kill();
end
#26 - RASa + RAF -> RASa + RAFa -- a2
dyn [RAF] [RASa] rate 1E-7 react
	spawn(1:RAFa);
	reagent_1.kill();
end
#27 - RAFa -> RAF -- b2
dyn [RAFa] rate 9E-3 react
	reagent_1.kill();
	spawn(1:RAF);
end
#28 - RAFa + MEK  -> MEKp + RAFa -- a3
dyn [MEK] [RAFa] rate 4E-7 react
	spawn(1:MEKp);
	reagent_1.kill();
end
#29 - MEKp -> MEK -- b3
dyn [MEKp] rate 1.8E-3 react
	reagent_1.kill();
	spawn(1:MEK);
end
#30 - ERK + MEKp -> MEKp + ERKp -- a4
dyn [ERK] [MEKp] rate 8E-07 react
	spawn(1:ERKp);
	reagent_1.kill();
end
#31 - ERKp -> ERK -- b4
dyn [ERKp] rate 2E-3 react
	reagent_1.kill();
	spawn(1:ERK);
end
#32 - ERKp -> ERKp + Myc -- Hill(a5,Kd,n1)
dyn [ERKp] rate 90/(2E3 + (count [ERKp])) react
	spawn(1:Myc);
end
#33 - Myc -> deg -- b5
dyn [Myc] rate 1.2E-2 react
	reagent_1.kill();
end
#34 - ERKp -> ERKp + CD -- Hill(a6,Ke,n1)
dyn [ERKp] rate 50/(2E3+(count [ERKp])) react
	spawn(1:CD);
end
#35 - Myc -> Myc + CD -- Hill(a6p,Kf,n1)
dyn [Myc] rate 90/(2E3+(count [Myc])) react
	spawn(1:CD);
end
#36 - CD -> deg -- b6
dyn [CD] rate 3.5E-2 react
	reagent_1.kill();
end
#37 - Myc -> Myc + E2F -- Hill(a10,Kg,n1)
dyn [Myc] rate 60/(2E3+(count [Myc])) react
	spawn(1:E2F);
end
#38 - CD + RE -> CD + E2F + RBp -- b7'
dyn [RE] [CD] rate 6E-6 react
	spawn(1:RBp);
	spawn(1:E2F);
	reagent_1.kill();
end
#39 - E2F -> deg -- b10
dyn [E2F] rate 4E-4 react
	reagent_1.kill();
end
#40 - E2F + RB -> RE -- a7
dyn [RB] [E2F] rate 1E-6 react
	spawn(1:RE);
	reagent_1.kill();
	reagent_2.kill();
end
#41 - I() -> I() + RB -- a9
dyn rate 40 react
	spawn(1:RB);
end
#42 - CD + RB -> CD + RBp -- a8
dyn [RB] [CD] rate 1E-5 react
	spawn(1:RBp);
end
#43 - RB -> deg -- b9
dyn [RB] rate 5E-4 react
	reagent_1.kill();
end
#44 - RBp -> RB -- b8
dyn [RBp] rate 6E-6 react
	spawn(1:RB);
	reagent_1.kill();
end
#45 - RBp -> deg -- b8'
dyn [RBp] rate 4E-4 react
	spawn(1:RB);
	reagent_1.kill();
end
#46 - RE -> deg -- b7
dyn [RE] rate 5E-4 react
	reagent_1.kill();
end
#47 - E2F -> E2F + CE -- Hill(a12,Ki,n1)
dyn [E2F] rate 170/(3E5+(count [E2F])) react
	spawn(1:CE);
	reagent_1.kill();
end
#48 - CE -> deg -- b12
dyn [CE] rate 3.5E-2 react
	reagent_1.kill();	
end
#49 - E2F -> E2F + ARF -- Hill(a11,Kh,n1)
dyn [E2F] rate 30/(3E5+(count [E2F])) react
	spawn(1:ARF);
end
#50 - ARF -> deg -- b11
dyn [ARF] rate 5E-3 react
	reagent_1.kill();
end
#51 - ARF + MDM2p -> ARF -- d7'
dyn [MDM2p] [ARF] rate 6.5E-6 react
	reagent_1.kill();
end
#52 - ARF + MDM2 -> ARF -- d7'
dyn [MDM2] [ARF] rate 6.5E-6 react
	reagent_1.kill();
end
#53 - P21 + CD -> P21 -- b6'
dyn [CD] [P21] rate 3E-7 react
	reagent_1.kill();
end
#54 - P21 + CE -> P21 -- b12'=b6'
dyn [CE] [P21] rate 3E-7 react
	reagent_1.kill();
end
#55 - I() -> I() + INK4A -- a13
dyn rate 40 react
	spawn(1:INK4A);
end
#56 - INK4A + CD -> INK4A -- b6''
dyn [CD] [INK4A] rate 3E-6 react
	reagent_1.kill();
end
#57 - INK4A -> deg -- b13
dyn [INK4A] rate 3E-3 react
	reagent_1.kill();
end
#58 - CE + RE -> CE + RBp + E2F -- b7'
dyn [RE] [CE] rate 6E-6 react
	spawn(1:E2F);
	spawn(1:RBp);
	reagent_1.kill();
end

run 1000:[1:RAGE] 10000:[1:RAS] 100000:[1:PI3K] 100000:[1:PIP2] 100000:[1:AKT] 10000:[1:RAF] 10000:[1:MEK] 10000:[1:ERK] 20000:[1:P53] 20000:[1:MDM2p] 10000:[1:MDM2] 100000:[1:RE]
