
#====BOXES===========================
x1	{}
x2	{}
x3	{}
x4	{}
x5	{}
x6	{}
x7	{}
x8	{}
x9	{}
x10	{}
x11	{}
x12	{}
x13	{}
x14	{}
x15	{}
x16	{}
x17	{}
x18	{}
x19	{}
x20	{}
# x21	{}
# x22	{}
# x23	{}
# x24	{}
# x25	{}
# x26	{}
# x27	{}
# x28	{}
# x29	{}
# x30	{}
# x31	{}
# x32	{}
# x33	{}
# x34	{}
# x35	{}
# x36	{}
# x37	{}
# x38	{}
# x39	{}
# x40	{}

#=====================NAMED PATTERNS======================
pattern P_x1 = [x1]
pattern P_x2 = [x2]
pattern P_x3 = [x3]
pattern P_x4 = [x4]
pattern P_x5 = [x5]
pattern P_x6 = [x6]
pattern P_x7 = [x7]
pattern P_x8 = [x8]
pattern P_x9 = [x9]
pattern P_x10 = [x10]
pattern P_x11 = [x11]
pattern P_x12 = [x12]
pattern P_x13 = [x13]
pattern P_x14 = [x14]
pattern P_x15 = [x15]
pattern P_x16 = [x16]
pattern P_x17 = [x17]
pattern P_x18 = [x18]
pattern P_x19 = [x19]
pattern P_x20 = [x20]
# pattern P_x21 = [x21]
# pattern P_x22 = [x22]
# pattern P_x23 = [x23]
# pattern P_x24 = [x24]
# pattern P_x25 = [x25]
# pattern P_x26 = [x26]
# pattern P_x27 = [x27]
# pattern P_x28 = [x28]
# pattern P_x29 = [x29]
# pattern P_x30 = [x30]
# pattern P_x31 = [x31]
# pattern P_x32 = [x32]
# pattern P_x33 = [x33]
# pattern P_x34 = [x34]
# pattern P_x35 = [x35]
# pattern P_x36 = [x36]
# pattern P_x37 = [x37]
# pattern P_x38 = [x38]
# pattern P_x39 = [x39]
# pattern P_x40 = [x40]

#print* P_u
#print* P_x1
#print* P_x2
#print* P_x3
#print* P_x4
#print* P_x5
#print* P_x6
#print* P_x7
#print* P_x8
#print* P_x9
#print* P_x10
#print* P_x11
#print* P_x12
#print* P_x13
#print* P_x14
#print* P_x15
#print* P_x16
#print* P_x17
#print* P_x18
#print* P_x19
#print* P_x20
# #print* P_x21
# #print* P_x22
# #print* P_x23
# #print* P_x24
# #print* P_x25
# #print* P_x26
# #print* P_x27
# #print* P_x28
# #print* P_x29
# #print* P_x30
# #print* P_x31
# #print* P_x32
# #print* P_x33
# #print* P_x34
# #print* P_x35
# #print* P_x36
# #print* P_x37
# #print* P_x38
# #print* P_x39
# #print* P_x40



#=====================HANDLERS============================

dyn [x1] [x2] rate	1E-04	 react 
	reagent_1.kill();  
	reagent_2.kill();  
	spawn(1:x3); 
end


dyn [x3] [x4] rate	1E-04	 react 
	reagent_1.kill();  
	reagent_2.kill();  
	spawn(1:x5); 
end


dyn [x5]  rate	1	 react 
	reagent_1.kill();  
	spawn(1:x6); 
end

dyn [x5]  rate	1	 react 
	reagent_1.kill();  
	spawn(1:x7); 
end

dyn [x8] [x6] rate	2E-04	 react 
	reagent_1.kill();  
	spawn(1:x10); 
end

dyn [x9] [x7] rate	2E-04	 react 
	reagent_1.kill();  
	spawn(1:x11); 
end

dyn [x10] [x6] rate	1	 react 
	reagent_1.kill();  
	spawn(1:x12); 
end

dyn [x11] [x7] rate	1	 react 
	reagent_1.kill();  
	spawn(1:x13); 
end

dyn [x12] [x6] rate	1	 react 
	reagent_1.kill();  
	spawn(1:x14); 
end

dyn [x13] [x7] rate	1	 react 
	reagent_1.kill();  
	spawn(1:x15); 
end

dyn [x14] [x6] rate	1	 react 
	reagent_1.kill();  
	spawn(1:x16); 
end

dyn [x15] [x7] rate	1	 react 
	reagent_1.kill();  
	spawn(1:x17); 
end

dyn [x16] [x6] rate	1	 react 
	reagent_1.kill();  
	spawn(1:x18); 
end

dyn [x17] [x7]  rate	1	 react 
	reagent_1.kill();  
	spawn(1:x19); 
end

dyn [x18] [x6] rate	1	 react 
	reagent_1.kill();  
	spawn(1:x20); 
end


run 100:[1:x1] 300:[1:x2] 1:[1:x4] 300:[1:x8] 100:[1:x9]