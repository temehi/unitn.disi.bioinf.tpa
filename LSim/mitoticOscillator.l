#cylin
C{}
#active.cdc2.kinase
M_act{}
#inactive.cdc2.kinase
M_ina{}
#active.cyclin.protease
X_act{}
#inactive.cyclin.protease
X_ina{}

pattern P_C = [C]
pattern P_M_act = [M_act]
pattern P_X_act = [X_act] 

#print* P_C
#print* P_M_act
#print* P_X_act

#creation.of.cyclin
dyn
rate custom 205
	react
		spawn(1:C);
end

#default.degradation.of.cyclin
dyn [C]
rate 1.667e-4
	react
		reagent_1.kill();
end

#cdc2.kinase.triggered.degration.of.cyclin
dyn [C][X_act]
rate custom (0.0042 * (count [X_act]) * (count [C])) / (3.011e3 + (count [C]))
	react
		reagent_1.kill();
end

#activation.of.cdc2.kinase
dyn [M_ina]
rate custom (((30110 * (count [C])) / (3.011e5 + (count [C]))) * (count [M_ina])) / (3.011e3 + (count [M_ina]))
	react
		reagent_1.kill();
		spawn(1:M_act);
end

#deactivation.of.cdc2.kinase
dyn [M_act]
rate custom (15055 * (count [M_act])) / (3011 + (count [M_act]))
	react
		reagent_1.kill();
		spawn(1:M_ina);
end

#activation.of.cyclin.protease
dyn [X_ina]
rate custom ((0.0167 * (count [M_act])) * (count [X_ina])) / (3.011e3 + (count [X_ina]))	
	react
		reagent_1.kill();
		spawn(1:X_act);
end

#deactivation.of.cyclin.protease
dyn [X_act]
rate custom (5.0183e3 * (count [X_act])) / (3011 + (count [X_act]))
	react
		reagent_1.kill();
		spawn(1:X_ina);
end

run 6022:[1:C] 6022:[1:M_act] 596200:[1:M_ina] 6022:[1:X_act] 596200:[1:X_ina]