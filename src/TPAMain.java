import java.io.IOException;

import java.io.*;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

public class TPAMain {
	public static int N = 10; // This is how many runs should we make to calculate the probability
	public static int INTERVAL = 10; // This is the interval we use to search for the turning point
	public static int MAX_SIM_TIME = 1000; //This is the time in the real world time to have the event.
	
	
	
	public static void main(String[] args) throws IOException {


		int posResCount = 0;
		int causeTime = 0;
		int noIntervals = MAX_SIM_TIME/INTERVAL;
		String intialState = "run 100:[1:x1] 300:[1:x2] 1:[1:x4] 300:[1:x8] 100:[1:x9]";
		String reactionPath = "LSim\\reactions.txt";
		String component = "P_x20";
		boolean isGeq = true;
		int value = 30;		
		try 
		{
			// Do the first set of simulation and take one positive trace
			String simResultFilePath = "";
			ClearDirectory("LSim\\model_results\\original\\positive");//Delete files from the previous run. 
			for (int i = 0; i < N; i++) { // Run N instances of the original simulation and 
											//identify the positive ones and collect them
				simResultFilePath = RunSimulation(reactionPath, intialState, MAX_SIM_TIME, i, true);
				if (CheckEvent(component, isGeq, value, simResultFilePath)) {
					posResCount++;
					MoveFile(simResultFilePath, "LSim\\model_results\\original\\positive\\model.patterns_"+i+".csv");
				}
			}
			
			System.out.println(posResCount + " out of "+ N +" runs show an occurence of the event."); 

			File folder = new File("LSim\\model_results\\original\\positive");
			File[] inputFiles = folder.listFiles();

			
			//Make a directory for the final result according to the run time
			DateFormat dateFormat = new SimpleDateFormat("MMMM_dd HH.mm.ss");
			String resDirPath = "LSim\\TPA_Results\\" + dateFormat.format(Calendar.getInstance().getTime());
			new File(resDirPath).mkdir();	
			
			//for each each positive traces
 			for (int f = 0; f < inputFiles.length; f++) 
			//for (int f = 0; f < 1; f++) 
			{
				File outputFile = new File(resDirPath +"\\result_" + f + ".csv");
				Writer output = new BufferedWriter(new FileWriter(outputFile));

				System.out.println("\nChecking the file " + inputFiles[f].getName());
				output.append("causeTime, posResCount\n");
				//for each interval point
				for (int i = 1; i < noIntervals ; i++) 
				{
					posResCount = 0;
					causeTime = i * INTERVAL;
					intialState = GetState(inputFiles[f].getAbsolutePath(), causeTime);
					for (int j = 0; j < N; j++){
						simResultFilePath = RunSimulation(reactionPath, intialState, (MAX_SIM_TIME - causeTime), i, false);
						if (CheckEvent(component, isGeq, value, simResultFilePath))
							posResCount ++;
					}
					output.append(causeTime + ", " + posResCount + "\n");
				}
				output.write("");
				// Close the output stream
				output.close();
			}
			//Return GetState (posTrace, CauseTime);

		} catch (Exception e) {// Catch exception if any
			System.out.println(e.getMessage());
		}
	}

	
	
	/**
	 * This function accepts the the name of the component, an output from LSim and an event in the form of
	 * Component_Count >= C or Component_Count <= C. 
	 * If the parameter isGeq is true and Value = C then our event is Component_Count >= C 
	 * If the parameter isGeq is false and Value = C then our event is Component_Count <= C 
	*/
	private static boolean CheckEvent(String component, boolean isGeq, int value, String simResultFilePath) {
		boolean found = false; 
		int columnNo = 0;
		try {
			File simResultFile = new File(simResultFilePath);			
			BufferedReader input = new BufferedReader(new FileReader(simResultFile));
			String line = input.readLine();
			for (String col : processLine(line, ","))
			{
				if (col.contains(component))
					break;
				columnNo++;
			}

			while ((line = input.readLine()) != null) {
				int val  = Integer.valueOf(processLine(line, ",")[columnNo]);
				if (isGeq && val>=value)
				{
					found = true;
					break;
				}
				if(!isGeq && val<=value)
				{
					found = true;
					break;
				}
			}
			input.close();
		}catch (IOException e) {
			e.printStackTrace();
		}
		return found;
	}

	/**This function accepts the l-specification of the reactions and their parameters,
	* The initial state of the components from which the simulations starts and 
	* The simulation time, for how long in the real world we should cover by simulation
	* The parameters runNo and is original are used for naming the result file and determine the 
	* location of the file respectively.
	*/
	
	private static String RunSimulation(String reactionPath, String intialState, int simTime, int runNO, boolean isOriginalRun) 
	{

		//Prepare the model's l-specification with respect to the given intialState
		//by appending the give initial state in the model
		try {
			String outputPath = "LSim\\model.l";
			File inputFile = new File(reactionPath);			
			File outputFile = new File(outputPath);
			BufferedReader input = new BufferedReader(new FileReader(inputFile));
			Writer output = new BufferedWriter(new FileWriter(outputFile));
			String line = "";
			while ((line = input.readLine()) != null) {
				output.append("\n" + line);
			}
			output.append("\n\n\n" + intialState);
			input.close();
			output.write("");
			// Close the output stream
			output.close();
			runLSim(1, simTime, " D:\\SimBio\\unitn.disi.bioinf.TPA\\LSim\\model.l");
			String resultPath = "LSim\\model_results\\model.patterns.csv";
			String newResultPath = "LSim\\model_results\\model.patterns_"+runNO+".csv";
			if (isOriginalRun)
				newResultPath = "LSim\\model_results\\original\\model.patterns_"+runNO+".csv";
			File resultFile = new File(resultPath);
			File newResultFile = new File(newResultPath);

			CopyFile(resultFile, newResultFile);
			return newResultPath;
		}catch (IOException e) {
			e.printStackTrace();
		}

		return "Simulation was not succesfull";

	}

	
	/**
	 * This function accepts an out put of the simulation and a specific time and returns 
	 * the state of the system at that times.
	 */
	
	private static String GetState(String posTracePath, int causeTime) {
		String state = "run "; 
		try {
			File simResultFile = new File(posTracePath);			
			BufferedReader input = new BufferedReader(new FileReader(simResultFile));
			String header = input.readLine();
			String line = "";

			while ((line = input.readLine()) != null) {
				double val  = Double.valueOf(processLine(line, ",")[0]);
				if (val >= causeTime)
					break;
			}

			input.close();
			String[] values = processLine(line, ",");
			String[] headers = processLine(header, ",");
			for(int i=1; i<headers.length; i++)
			{
				state += values[i] +":[1:" + headers[i].replace("P_", "") + "] ";
			}		
		}catch (IOException e) {
			e.printStackTrace();
		}
		return state;
	}
	
	/**
	 * This Function runs the L-Simulator based on the following parameters
	 * @param resolution :The time resolution used to output results
	 * @param time : A termination criterion, the maximum simulation time before stopping the simulation.
	 * @param modelPath: the path of the model written according to the L-specification
	 */
	public static void runLSim(int resolution, int time, String modelPath)
	{

		//	LSim.exe -r:60 -R:results -steps:1000000 -time, string tamp+ IL6_pathway_model.l
		try{
			String command = "D:\\SimBio\\unitn.disi.bioinf.TPA\\LSim\\LSim.exe -r:" + resolution + " -R:LSim\\model_results\\ -time:" + time + modelPath; 
			Runtime rt = Runtime.getRuntime();
			Process p = rt.exec(command);
			p.waitFor();
		}
		catch (Exception ex)
		{
		}

	}
	
	
	
	
	//-------------------------------Auxiliary Functions--------------------------
	
	
	/**
	 * This function accepts a string and returns an array of strings
	 * by splitting the original string using a delimiter provided.
	 */

	protected static String[] processLine(String aLine, String delimiter) {
		String wordList = "";
		// use a second Scanner to parse the content of each line
		Scanner scanner = new Scanner(aLine);
		scanner.useDelimiter(delimiter);
		String temp = "";
		scanner.skip("");
		while (scanner.hasNext()) {
			temp = scanner.next();
			if (temp.length() > 0)
				wordList += temp + ":";
		}
		return wordList.split(":");
		// no need to call scanner.close(), since the source is a String
	}

	/**
	 * Copies a file from one place to another
	 * @param sourceFile
	 * @param destFile
	 * @throws IOException
	 */
	public static void CopyFile(File sourceFile, File destFile) throws IOException {
		if(!destFile.exists()) {
			destFile.createNewFile();
		}

		FileChannel source = null;
		FileChannel destination = null;

		try {
			source = new FileInputStream(sourceFile).getChannel();
			destination = new FileOutputStream(destFile).getChannel();
			destination.transferFrom(source, 0, source.size());
		}
		finally {
			if(source != null) {
				source.close();
			}
			if(destination != null) {
				destination.close();
			}
		}
	}

	/**
	 * Moves a file
	 * @param sourceFilePath
	 * @param destFilePath
	 */
	public static void MoveFile(String sourceFilePath, String destFilePath) {
		// File (or directory) to be moved
		try
		{
			File source = new File(sourceFilePath);

			// Destination directory
			File dest = new File(destFilePath);

			// Move file to new directory
			source.renameTo(dest);
		}catch (Exception e) {
			System.out.println("unable to move");
		}
	}


	/**
	 * Deletes all the files in the folder
	 * @param folderPath
	 */
	public static void ClearDirectory(String folderPath) {
		// File (or directory) to be moved
		try
		{
			File folder = new File(folderPath);
			File[] inputFiles = folder.listFiles();
			for (int f = 0; f < inputFiles.length; f++) 
			{
				inputFiles[f].delete();
			}
		}catch (Exception e) {
			System.out.println("unable to delete");
		}
	}
}
