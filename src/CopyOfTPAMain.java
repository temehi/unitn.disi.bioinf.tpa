import java.io.IOException;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.Scanner;

public class CopyOfTPAMain {
	public static int N = 10;
	public static double MIN_INTERVAL = 1;
	public static void main(String[] args) throws IOException {


		//	Input: 	Set of reactions (R)
		//			Initial State (I)
		//			Event of Interest (E) [E.time, E.Condition] 
		//			//E.time is the time at which the event should happen.
		//			No of Simulation (N)
		//			MIN_INTERVAL 

		int maxSimTime = 1000;
		int posResCount = 0;
		int causeTime = 0;
		String intialState = "run 100:[1:x1] 300:[1:x2] 1:[1:x4] 300:[1:x8] 100:[1:x9]";
		String reactionPath = "LSim\\reactions.txt";
		String component = "P_x20";
		boolean isGeq = true;
		int value = 30;		
		try 
		{
			// Do the first set of simulation and take one positive trace
			String simResultFilePath = "";
			ClearDirectory("LSim\\model_results\\original\\positive");
			for (int i = 0; i < N; i++) {
				simResultFilePath = RunSimulation(reactionPath, intialState, (maxSimTime - causeTime), i, true);
				if (CheckEvent(component, isGeq, value, simResultFilePath)) {
					posResCount++;
					MoveFile(simResultFilePath, "LSim\\model_results\\original\\positive\\model.patterns_"+i+".csv");
				}
			}
			System.out.println(posResCount + " out of "+ N +" runs show an occurence of the event."); 


			File folder = new File("LSim\\model_results\\original\\positive");
			File[] inputFiles = folder.listFiles();
			for (int f = 0; f < inputFiles.length; f++) 
			{
				System.out.println("\nChecking the file " + inputFiles[f].getName());
				int upperBound = maxSimTime;
				int lowerBound = 0;
				while (true){
					posResCount = 0;
					causeTime = (upperBound + lowerBound)/2;
					intialState = GetState(inputFiles[f].getAbsolutePath(), causeTime);
					System.out.println("\nUpper Bound: " + upperBound+ "\tLower Bound: " + lowerBound+ "\tChecking time: " + causeTime);
					System.out.println("\nIntial State: " + intialState);
					for (int i = 0; i < N; i++){
						simResultFilePath = RunSimulation(reactionPath, intialState, (maxSimTime - causeTime), i, false);
						if (CheckEvent(component, isGeq, value, simResultFilePath))
							posResCount ++;
					}
					if(posResCount/N == 1) 
						upperBound = causeTime;
					else
						lowerBound = causeTime;

					if((upperBound - lowerBound) <= MIN_INTERVAL) {
						causeTime = upperBound;
						break;
					}
				}					
			}


			//Return GetState (posTrace, CauseTime);

		} catch (Exception e) {// Catch exception if any
			System.out.println(e.getMessage());
		}
	}

	private static boolean CheckEvent(String component, boolean isGeq, int value, String simResultFilePath) {
		boolean found = false; 
		int columnNo = 0;
		try {
			File simResultFile = new File(simResultFilePath);			
			BufferedReader input = new BufferedReader(new FileReader(simResultFile));
			String line = input.readLine();
			for (String col : processLine(line, ","))
			{
				if (col.contains(component))
					break;
				columnNo++;
			}

			while ((line = input.readLine()) != null) {
				int val  = Integer.valueOf(processLine(line, ",")[columnNo]);
				if (isGeq && val>=value)
				{
					found = true;
					break;
				}
				if(!isGeq && val<=value)
				{
					found = true;
					break;
				}
			}
			input.close();
		}catch (IOException e) {
			e.printStackTrace();
		}
		return found;
	}

	private static String RunSimulation(String reactionPath, String intialState,	int simTime, int runNO, boolean isOriginalRun) 
	{

		//Prepare the model's l-specification with respect to the given intialState
		//by appending the give initial state in the model
		try {
			String outputPath = "LSim\\model.l";
			File inputFile = new File(reactionPath);			
			File outputFile = new File(outputPath);
			BufferedReader input = new BufferedReader(new FileReader(inputFile));
			Writer output = new BufferedWriter(new FileWriter(outputFile));
			String line = "";
			while ((line = input.readLine()) != null) {
				output.append("\n" + line);
			}
			output.append("\n\n\n" + intialState);
			input.close();
			output.write("");
			// Close the input stream
			output.close();
			runLSim(1, simTime, " D:\\SimBio\\unitn.disi.bioinf.TPA\\LSim\\model.l");
			String resultPath = "LSim\\model_results\\model.patterns.csv";
			String newResultPath = "LSim\\model_results\\model.patterns_"+runNO+".csv";
			if (isOriginalRun)
				newResultPath = "LSim\\model_results\\original\\model.patterns_"+runNO+".csv";
			File resultFile = new File(resultPath);
			File newResultFile = new File(newResultPath);

			CopyFile(resultFile, newResultFile);
			return newResultPath;
		}catch (IOException e) {
			e.printStackTrace();
		}

		return "Simulation was not succesfull";

	}

	private static String GetState(String posTracePath, int causeTime) {
		String state = "run "; 
		try {
			File simResultFile = new File(posTracePath);			
			BufferedReader input = new BufferedReader(new FileReader(simResultFile));
			String header = input.readLine();
			String line = "";

			while ((line = input.readLine()) != null) {
				double val  = Double.valueOf(processLine(line, ",")[0]);
				if (val >= causeTime)
					break;
			}
			
			input.close();
			String[] values = processLine(line, ",");
			String[] headers = processLine(header, ",");
			for(int i=1; i<headers.length; i++)
			{
 					state += values[i] +":[1:" + headers[i].replace("P_", "") + "] ";
			}		
		}catch (IOException e) {
			e.printStackTrace();
		}
		return state;
	}
	public static void runLSim(int resolution, int time, String modelPath)
	{

		//	LSim.exe -r:60 -R:results -steps:1000000 -time, string tamp+ IL6_pathway_model.l
		try{
			String command = "D:\\SimBio\\unitn.disi.bioinf.TPA\\LSim\\LSim.exe -r:" + resolution + " -R:LSim\\model_results\\ -time:" + time + modelPath; 
			Runtime rt = Runtime.getRuntime();
			Process p = rt.exec(command);
			p.waitFor();
		}
		catch (Exception ex)
		{
		}

	}
	public static void CopyFile(File sourceFile, File destFile) throws IOException {
		if(!destFile.exists()) {
			destFile.createNewFile();
		}

		FileChannel source = null;
		FileChannel destination = null;

		try {
			source = new FileInputStream(sourceFile).getChannel();
			destination = new FileOutputStream(destFile).getChannel();
			destination.transferFrom(source, 0, source.size());
		}
		finally {
			if(source != null) {
				source.close();
			}
			if(destination != null) {
				destination.close();
			}
		}
	}

	public static void MoveFile(String sourceFilePath, String destFilePath) {
		// File (or directory) to be moved
		try
		{
			File source = new File(sourceFilePath);

			// Destination directory
			File dest = new File(destFilePath);

			// Move file to new directory
			source.renameTo(dest);
		}catch (Exception e) {
			System.out.println("unable to move");
		}
	}


	public static void ClearDirectory(String folderPath) {
		// File (or directory) to be moved
		try
		{
			File folder = new File(folderPath);
			File[] inputFiles = folder.listFiles();
			for (int f = 0; f < inputFiles.length; f++) 
			{
				inputFiles[f].delete();
			}
		}catch (Exception e) {
			System.out.println("unable to delete");
		}
	}









	/**
	 * This function accepts a string and returns an array of strings
	 * by splitting the original string using a delimiter provided.
	 */

	protected static String[] processLine(String aLine, String delimiter) {
		String wordList = "";
		// use a second Scanner to parse the content of each line
		Scanner scanner = new Scanner(aLine);
		scanner.useDelimiter(delimiter);
		String temp = "";
		scanner.skip("");
		while (scanner.hasNext()) {
			temp = scanner.next();
			if (temp.length() > 0)
				wordList += temp + ":";
		}
		return wordList.split(":");
		// no need to call scanner.close(), since the source is a String
	}








}
